$(document).ready(function() {
    //_____________________________form validations
    var patternEmail =/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
    var patternPhone=/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,15}(\s*)?$/;
    $('input').focus(function () {
        $(this).css('border: 1px solid #d9d7d7;');
    })

    $('.form').submit( function (event) {
        
        event.preventDefault();
        var mfirstName = $("#form").find('input[name="firstName"]').val();
        var mlastName = $("#form").find('input[name="lastName"]').val();
        var madress = $("#form").find('input[name="adress"]').val();
        var mpostalCode = $("#form").find('input[name="postalCode"]').val();
        var mcity = $("#form").find('input[name="city"]').val();
        var mcountry = $("#form").find('input[name="country"]').val();
        var mphone = $("#form").find('input[name="phone"]').val();
        var memail = $("#form").find('input[name="mail"]').val();

        console.log(mfirstName);
        
// var inputName=$('.header-form-top input[name="name"]').val();
// var inputPhone=$('.header-form-top input[name="phone"]').val();
//console.log(mname);
        if(!patternPhone.test(mphone) || mphone=='') {
            $('input[name="phone"]').css('border','1px solid red');
        }
        if(!patternEmail.test(memail) || memail=='') {
            $('input[name="mail"]').css('border','1px solid red');
        }
        if(mfirstName =="") {
            $('input[name="firstName"]').css('border','1px solid red');
        }
        if(mlastName =="") {
            $('input[name="lastName"]').css('border','1px solid red');
        }
        if(madress =="") {
            $('input[name="adress"]').css('border','1px solid red');
        }
        if(mpostalCode =="") {
            $('input[name="postalCode"]').css('border','1px solid red');
        }
        if(mcity =="") {
            $('input[name="city"]').css('border','1px solid red');
        }
        if(mcountry =="") {
            $('input[name="country"]').css('border','1px solid red');
        }
        if(mfirstName && mphone && memail && patternPhone.test(mphone) && patternEmail.test(memail)  ){
            console.log("hello");
            var th = $(this);
            $.ajax({
                type: "POST",
                url: "mail.php", //Change
                data: th.serialize()
            }).done(function() {
                $('.overlayFormMember').fadeIn();
                // $('.js-overlay-campaign').fadeIn();
                // $('.js-overlay-campaign').addClass('disabled');
                setTimeout(function() {
                    // Done Functions
                    // $('.js-overlay-campaign').fadeOut();
                    th.trigger("reset");
                }, 3500);
            });
            return false;
        }
    });

    
    //_____________________________form validations end
    
    $("#cardType").change(function(){
        var cardSelection = $(this).val();
        $("#imgCardType").attr('src', 'img/svg/' + cardSelection + '.svg');
        $("#card").val('');
    })
    //____________________________card number
    var cards = {
        37:{0:"American Express",1:"",2:"y",3:15,4:"l"},
        4:{0:"Visa",1:"",2:"y",3:""},
        4026:{0:"Visa Electron",1:"",2:"y",3:16,4:"l"},
        417500:{0:"Visa Electron",1:"",2:"y",3:16,4:"l"},
        4405:{0:"Visa Electron",1:"",2:"y",3:16,4:"l"},
        4508:{0:"Visa Electron",1:"",2:"y",3:16,4:"l"},
        4844:{0:"Visa Electron",1:"",2:"y",3:16,4:"l"},
        4913:{0:"Visa Electron",1:"",2:"y",3:16,4:"l"},
        4917:{0:"Visa Electron",1:"",2:"y",3:16,4:"l"},
        5:{0:"Mastercard",1:"",2:"y",3:16,4:"l"},
        56:{0:"Maestro",1:"",2:"y",3:""},
        63:{0:"Maestro",1:"",2:"y",3:""},
        65:{0:"Discover Card",1:"",2:"y",3:""},
        67:{0:"Maestro",1:"",2:"y",3:16,4:"l"},
      };
      
    var cardlen = "";
    var cardchk = "";

    $("#card").bind("propertychange input paste", function() {


    cardno = $("#card").val();
    if ((cardno == null) || (cardno == undefined) || (cardno == "")) {
      cardtype = "";
      $("body").removeClass("invalid");
      $("#card").removeAttr('maxlength');
    } else if ((cards[cardno] != null) || (cards[cardno] != undefined)) {
      cardtype = cards[cardno][0];
      if ((cards[cardno][1] != null) && (cards[cardno][1] != "") && (cards[cardno][1] != undefined)) {
        cardtype = cards[cardno][1];
      }
      cardlen = cards[cardno][3];
      $("#card").attr('maxlength',cardlen);
      cardchk = cards[cardno][4];
      if(cards[cardno][2] == "n") {
        $("body").addClass("invalid");
        $("#card").removeAttr('maxlength');
      } else {
        $("body").removeClass("invalid");
      }
    }

    // @todo - Add Proper length checks
    if ((cardno.length == cardlen)&&(cardlen != "")) {
        alert("Correct Card Number Length");
        // @ todo - Add Luhn Checking
        if ((cardchk == "l")) {
          alert("Perfom Luhn Check");
        }
    }

    $("#imgCardType").attr('src', './img/svg/' + cardtype + '.svg');
    $("#cardType").val(cardtype);
    });

    //____________________________card number end



    //____________________________popUp form

    $('.btnSubmit').click(function (e) {
        // e.preventDefault();
        // $('.overlayFormMember').fadeIn();
        // $('.member').addClass('blur');
        // $('.footer').addClass('blur');
    });
    
    $('.btnClosse').click(function () {
        $('.overlayFormMember').fadeOut();
        $('.member').removeClass('blur');
        $('.footer').removeClass('blur');
    });
    $(document).mouseup(function (e) {
        var container = $('.overlayFormMember');
        if(container.has(e.target).length === 0) {
            container.hide();
            $('.member').removeClass('blur');
            $('.footer').removeClass('blur');
        }
    });
    
    //____________________________popUp form end




    $( "#privacy" ).click(function() {
        if($(this).is(":checked")) {
            $('.btnSubmit').removeAttr('disabled');
        }
        else {
            $('.btnSubmit').attr("disabled", "");
        }
    });

    var head = $('.header');
    // if(head.offset().top >= 100) {
    //     $('.header').addClass('sticky');
    //     $('.wrapMenu').addClass('stickys');
    //     $('.text-slide').addClass('stickyLink');
    //     $('.wrapLogo').addClass('stickyLogo');
    // }
    $(document).scroll (function(){
        if($(this).scrollTop() >= 100) {
            $('.header').addClass('sticky');
            $('.wrapMenu').addClass('stickys');
            $('.text-slide').addClass('stickyLink');
            $('.wrapLogo').addClass('stickyLogo');

        } else {
            $('.header').removeClass('sticky');
            $('.wrapMenu').removeClass('stickys');
            $('.text-slide').removeClass('stickyLink');
            $('.wrapLogo').removeClass('stickyLogo');
        }
    });

    $( ".menu-icon" ).click(function() {
        $( this ).toggleClass( "effect1" );
        $('.wrapMenu').slideToggle();
    });

    $(function(){
        $('a[data-target^="anchor"]').bind('click', function(e){
            if($(document).width() < 1200) {
                $('.wrapMenu').slideUp(500);
                // $('.header').toggleClass('stickys');
            };

            var target = $(this).attr('href'),
                offset = $('.header').height() - 125,
                bl_top = $(target).offset().top - offset-30;

            bl_top_correct = $(target).offset().top - offset - 60;
            bl_top_correct2 = $(target).offset().top - offset - 30;
            $('body, html').animate({scrollTop: bl_top}, 800);
            $('body, html').animate({scrollTop: bl_top_correct}, 200);
            $('body, html').animate({scrollTop: bl_top_correct2}, 300);
            e.preventDefault();
            return false;
        });
    });

    $(function(){
        $('a[data-target^="scrollDown"]').bind('click', function(e){

            var target = $(this).attr('href'),
                offset = $('.header').height() - 355,
                bl_top = $(target).offset().top - offset-30;

            bl_top_correct = $(target).offset().top - offset - 60;
            bl_top_correct2 = $(target).offset().top - offset - 30;
            $('body, html').animate({scrollTop: bl_top}, 800);
            $('body, html').animate({scrollTop: bl_top_correct}, 200);
            $('body, html').animate({scrollTop: bl_top_correct2}, 300);
            e.preventDefault();
            return false;
        });
    });

    const $photosCounterFirstSpan = $(".photos-counter span:nth-child(1)");
    const $gl = $(".sliderBook");

    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1200,
        autoplay: true,
        arrow: true,
    });
    $('.sliderBook').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        speed: 1200,
        autoplay: true,
        arrow: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrow: false
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
    });

    // gl.on("init", (event, slick) => {
    //     $photosCounterFirstSpan.text(`${slick.currentSlide + 1}/`);
    //     $(".photos-counter span:nth-child(2)").text(slick.slideCount);
    // });

    // $gl.on("afterChange", (event, slick, currentSlide) => {
    //     $photosCounterFirstSpan.text(`${slick.currentSlide + 1}/`);
    // });

    // $(".sliderBook .item").on("click", function() {
    //     const index = $(this).attr("data-slick-index");
    //     $gl.slick("slickGoTo", index);
    // });

    // $('.btn-buy').click(function(e){
    //     e.preventDefault();
    //
    //     $('.btn-close-payment-frame').css('display', 'inline');
    //     $(e.target.dataset.target).css('display', 'block');
    // });
    // $('.btn-close-payment-frame').click(function (e) {
    //     e.preventDefault();
    //     $('.btn-close-payment-frame').css('display', 'none');
    //     $('.payment-frame').css('display', 'none');
    // })

});


